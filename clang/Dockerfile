ARG LLVM_VERSION
FROM musicscience37/sphinx-doxygen:clang${LLVM_VERSION}

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    # SSH
    openssh-client \
    # GPG
    gnupg2 \
    # Text editors
    vim nano \
    # For bibtex used in Doxygen.
    texlive-base \
    # For ps2pdf used in pprof.
    ghostscript \
    # GDB
    gdb \
    # npm for cspell
    npm \
    && \
    apt-get autoremove && \
    apt-get autoclean && \
    rm -r /var/lib/apt/lists/* && \
    # cspell
    npm install -g cspell

# IWYU
ARG LLVM_VERSION
ARG IWYU_BRANCH
WORKDIR /root
RUN git clone https://github.com/include-what-you-use/include-what-you-use.git -b ${IWYU_BRANCH} && \
    cd include-what-you-use && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_PREFIX_PATH=/usr/lib/llvm-${LLVM_VERSION} .. && \
    cmake --build . && \
    cmake --build . --target install && \
    cd .. && \
    rm -rf build

# gperftools
WORKDIR /root
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    autoconf automake libtool && \
    apt-get autoremove && \
    apt-get autoclean && \
    rm -r /var/lib/apt/lists/* && \
    git clone https://github.com/gperftools/gperftools.git -b gperftools-2.16 && \
    cd gperftools && \
    ./autogen.sh && \
    ./configure CC=clang CXX=clang++ CXXFLAGS=-stdlib=libc++ --disable-libunwind --enable-frame-pointers && \
    make && \
    make install && \
    cd .. && \
    rm -rf gperftools

WORKDIR /root
