#!/bin/bash -e

set -e

echo "> python version"
python --version

echo "-- version of pip3 -------------------------"
pip3 --version

echo "> pipenv version"
pipenv --version

echo "-- version of wget -------------------------"
wget --version

echo "-- version of curl -------------------------"
curl --version

echo "-- version of perl -------------------------"
perl --version

echo "-- version of git --------------------------"
git --version

echo "-- version of make -------------------------"
make --version

echo "-- version of ninja ------------------------"
ninja --version

echo "-- version of cmake ------------------------"
cmake --version

echo "-- version of valgrind ---------------------"
valgrind --version

echo "-- version of ccache -----------------------"
ccache --version

echo "-- version of clnag ------------------------"
clang --version

echo "-- version of clnag++ ----------------------"
clang++ --version

echo "-- version of clnagd -----------------------"
clangd --version

echo "-- version of llvm-cov ---------------------"
llvm-cov --version

echo "-- version of java -------------------------"
java --version

echo "-- check of dot ----------------------------"
dot -?

echo "-- version of PlantUML ---------------------"
java -jar ${PLANTUML_JAR_PATH} -version

echo "-- version of doxygen ----------------------"
doxygen --version

echo "-- version of vim --------------------------"
vim --version

echo "-- version of nano -------------------------"
nano --version

echo "-- version of cspell -----------------------"
cspell --version

echo "-- version of include-what-you-use ---------"
include-what-you-use --version

echo "-- check of iwyu_tool.py -------------------"
iwyu_tool.py -h

echo "-- check of fix_includes.py ----------------"
fix_includes.py -h

echo "-- check of pprof --------------------------"
pprof --version

echo "-- check of libprofiler.a ------------------"
ls -l /usr/local/lib/libprofiler.a
